package br.com.jakarta.activities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCGLSurfaceView;

import br.com.jakarta.scenes.TitleScreen;
import br.com.jakarta.utils.DeviceSettings;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        CCGLSurfaceView glSurfaceView = new CCGLSurfaceView(this);
        setContentView(glSurfaceView);
        CCDirector.sharedDirector().attachInView(glSurfaceView);

        //open screen main
        CCScene scene = new TitleScreen().scene();
        CCDirector.sharedDirector().runWithScene(scene);

        configSensorManager();
    }


    private void configSensorManager(){
        SensorManager sensor = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        DeviceSettings.setSensorManager(sensor);
    }
}
