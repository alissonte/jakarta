package br.com.jakarta.utils;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import br.com.jakarta.delegate.AccelerometerDelegate;

public class Accelerometer implements SensorEventListener{
    private static Accelerometer sharedAccelerometer;

    private AccelerometerDelegate delegate;
    private SensorManager sensorManager;

    private float currentAccelerationX;
    private float currentAccelerationY;
    private float calibratedAccelerationX;
    private float calibratedAccelerationY;

    private int calibrated;

    public Accelerometer(){
        cacthAccelerometer();
    }

    public static Accelerometer sharedAccelerometer(){
        if(sharedAccelerometer == null){
            sharedAccelerometer = new Accelerometer();
        }
        return sharedAccelerometer;
    }

    @Override
    public void onSensorChanged(SensorEvent acceleration) {
        if(calibrated < 100){
            calibratedAccelerationX += acceleration.values[0];
            calibratedAccelerationY += acceleration.values[1];

            calibrated++;

            if(calibrated == 100){
                calibratedAccelerationX /= 100;
                calibratedAccelerationY /= 100;
            }
            return;
        }
        currentAccelerationX = acceleration.values[0] - calibratedAccelerationX;
        currentAccelerationY = acceleration.values[1] - calibratedAccelerationY;

        if(delegate != null){
            delegate.accelerometerDidAccelerate(currentAccelerationX, currentAccelerationY);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void setDelegate(AccelerometerDelegate delegate) {
        this.delegate = delegate;
    }

    public AccelerometerDelegate getDelegate() {
        return delegate;
    }

    public void cacthAccelerometer(){
        sensorManager = DeviceSettings.getSensorManager();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                       SensorManager.SENSOR_DELAY_GAME);
    }
}
