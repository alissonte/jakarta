package br.com.jakarta.utils;

import android.hardware.SensorManager;

import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGSize;

public class DeviceSettings {

    public static final float BACK_WIDTH = 252.63f;
    public static final float BACK_HEIGHT = 480f;

    private static SensorManager sensorManager;

    public static CGPoint screenResolution(CGPoint cgPoint){
        return cgPoint;
    }

    public static float screenWidth(){
        return winSize().getWidth();
    }

    public static float screenHeight(){
        return winSize().getHeight();
    }

    public static CGSize winSize(){
        return CCDirector.sharedDirector().winSize();
    }

    public static float scaleX(){
        return winSize().getWidth() / 480f;
    }

    public static float scaleY(){
        return winSize().getHeight() / 320f;
    }

    public static void setSensorManager(SensorManager sensor) {
        DeviceSettings.sensorManager = sensor;
    }

    public static SensorManager getSensorManager() {
        return sensorManager;
    }

    public static float aspectScale(CCSprite sprite, float scaleX, float scaleY)
    {
        float sourceWidth = sprite.getContentSize().width;
        float sourceHeight = sprite.getContentSize().height;

        float targetWidth = sourceWidth * scaleX;
        float targetHeight = sourceHeight * scaleY;
        float scalex = targetWidth / sourceWidth;
        float scaley = targetHeight / sourceHeight;

        return Math.min(scalex, scaley);
    }

}
