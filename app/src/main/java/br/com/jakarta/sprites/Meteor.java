package br.com.jakarta.sprites;


import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCFadeOut;
import org.cocos2d.actions.interval.CCScaleBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;

import java.util.Random;

import br.com.jakarta.R;
import br.com.jakarta.delegate.MeteorsEngineDelegate;

import static br.com.jakarta.utils.DeviceSettings.screenHeight;
import static br.com.jakarta.utils.DeviceSettings.screenResolution;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class Meteor extends CCSprite{

    private MeteorsEngineDelegate delegate;
    private float x, y;

    public Meteor(String image){
        super(image);
        x = new Random().nextInt(Math.round(screenWidth())) / 2.0f;
        y = screenHeight();
    }

    public void start(){
        this.schedule("update");
    }

    public void shooted(){
        float dt = 0.2f;
        CCScaleBy a1;
        CCFadeOut a2;
        CCSpawn s1;
        CCCallFunc c1;

        SoundEngine.sharedEngine().playEffect(CCDirector.sharedDirector().getActivity(),
                R.raw.bang);

        delegate.removeMeteor(this);

        a1 = CCScaleBy.action(dt, 0.5f);
        a2 = CCFadeOut.action(dt);
        s1 = CCSpawn.actions(a1, a2);

        c1 = CCCallFunc.action(this, "removeMe");
        runAction(CCSequence.actions(s1, c1));

        unschedule("update");
    }

    public void removeMe(){
        removeFromParentAndCleanup(true);
    }

    public void update(float dt){
        y -= 1;
        this.setPosition(screenResolution(CGPoint.ccp(x, y)));
    }

    public void setDelegate(MeteorsEngineDelegate delegate) {
        this.delegate = delegate;
    }
}
