package br.com.jakarta.sprites;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCFadeOut;
import org.cocos2d.actions.interval.CCScaleBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;

import br.com.jakarta.R;
import br.com.jakarta.delegate.ShootEngineDelegate;
import br.com.jakarta.utils.Assets;

import static br.com.jakarta.utils.DeviceSettings.screenResolution;

public class Shoot extends CCSprite{

    private ShootEngineDelegate delegate;
    private float positionX, positionY;

    public Shoot(float positionX, float positionY){
        super(Assets.SHOOT);
        this.positionX = positionX;
        this.positionY = positionY;

        setPosition(positionX, positionY);
        schedule("update");
    }

    public void update(float dt){
        positionY += 2;
        setPosition(screenResolution(CGPoint.ccp(positionX, positionY)));
    }

    public void explode(){
        delegate.removeShoot(this);
        unschedule("update");

        //efeitos
        float dt = 0.2f;
        CCScaleBy a1 = CCScaleBy.action(dt, 2f);
        CCFadeOut a2 = CCFadeOut.action(dt);
        CCSpawn s1 = CCSpawn.actions(a1, a2);

        CCCallFunc c1 = CCCallFunc.action(this, "removeMe");

        runAction(CCSequence.actions(s1, c1));
    }

    public void removeMe(){
        removeFromParentAndCleanup(true);
    }

    public void setDelegate(ShootEngineDelegate delegate) {
        this.delegate = delegate;
    }

    public void start(){
        SoundEngine.sharedEngine().playEffect(CCDirector.sharedDirector().getActivity(), R.raw.shoot);
    }
}