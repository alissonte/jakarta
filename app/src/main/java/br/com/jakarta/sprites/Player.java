package br.com.jakarta.sprites;

import org.cocos2d.actions.interval.CCFadeOut;
import org.cocos2d.actions.interval.CCScaleBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;

import br.com.jakarta.R;
import br.com.jakarta.delegate.AccelerometerDelegate;
import br.com.jakarta.delegate.GameOverScreenDelegate;
import br.com.jakarta.delegate.ShootEngineDelegate;
import br.com.jakarta.utils.Accelerometer;
import br.com.jakarta.utils.Assets;

import static br.com.jakarta.utils.DeviceSettings.screenHeight;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class Player extends CCSprite implements AccelerometerDelegate{

    private float positionX = screenWidth() / 2.0f;
    private float positionY = screenHeight() / 9.0f;
    public static final int NOISE = 1;

    private float currentAccelerometerX;
    private float currentAccelerometerY;

    private ShootEngineDelegate delegate;
    private GameOverScreenDelegate gameOverDelegate;
    private Accelerometer accelerometer;

    public Player(){
        super(Assets.NAVE);
        setPosition(positionX, positionY);
        setRotation(-180.f);
        schedule("update");
    }

    public void setDelegate(ShootEngineDelegate engineDelegate){
        this.delegate = engineDelegate;
    }

    public void moveLeft(){
        if(positionX > 30){
            positionX -= 10;
        }
        setPosition(positionX, positionY);
    }

    public void explode(){
        float dt = 0.2f;
        CCScaleBy a1;
        CCFadeOut a2;
        CCSpawn s1;

        SoundEngine.sharedEngine().playEffect(CCDirector.sharedDirector().getActivity(),
                R.raw.over);

        unschedule("update");

        //efeitos
        a1 = CCScaleBy.action(dt, 2f);
        a2 = CCFadeOut.action(dt);
        s1 = CCSpawn.actions(a1, a2);

        runAction(CCSequence.actions(s1));
        gameOverDelegate.startGameOverScreen();
    }

    public void setGameOverDelegate(GameOverScreenDelegate gameOverDelegate) {
        this.gameOverDelegate = gameOverDelegate;
    }

    public void moveRight(){
        if(positionX < screenWidth() - 30){
            positionX += 10;
        }
        setPosition(positionX, positionY);
    }

    public void update(float dt){
        if(currentAccelerometerX < -NOISE)
            this.positionX++;
        if(currentAccelerometerX > NOISE)
            this.positionX--;
        if(currentAccelerometerY < -NOISE)
            this.positionY++;
        if(currentAccelerometerY > NOISE)
            this.positionY--;

        setPosition(CGPoint.ccp(positionX, positionY));
    }

    public void shoot(){
        delegate.createShoot(new Shoot(positionX, positionY));
    }

    @Override
    public void accelerometerDidAccelerate(float x, float y) {
        currentAccelerometerX = x;
        currentAccelerometerY = y;
    }

    public void catchAccelerometer(){
        accelerometer = Accelerometer.sharedAccelerometer();
        accelerometer.cacthAccelerometer();
        accelerometer.setDelegate(this);
    }
}
