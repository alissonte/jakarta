package br.com.jakarta.delegate;
import br.com.jakarta.sprites.Meteor;

public interface MeteorsEngineDelegate {
    void createMeteor(Meteor meteor);
    void removeMeteor(Meteor meteor);
}
