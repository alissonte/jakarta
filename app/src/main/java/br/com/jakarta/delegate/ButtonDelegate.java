package br.com.jakarta.delegate;
import br.com.jakarta.layers.Button;

public interface ButtonDelegate {
    void buttonClicked(Button sender);
}
