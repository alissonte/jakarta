package br.com.jakarta.delegate;

import br.com.jakarta.sprites.Shoot;

public interface ShootEngineDelegate {
    void createShoot(Shoot shoot);
    void removeShoot(Shoot shoot);
}
