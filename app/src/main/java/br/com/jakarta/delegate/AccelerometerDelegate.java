package br.com.jakarta.delegate;

public interface AccelerometerDelegate {
    void accelerometerDidAccelerate(float x, float y);
}
