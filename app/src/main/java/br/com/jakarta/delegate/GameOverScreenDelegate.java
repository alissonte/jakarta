package br.com.jakarta.delegate;

public interface GameOverScreenDelegate {
    void startGameOverScreen();
}
