package br.com.jakarta.scenes;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import br.com.jakarta.R;
import br.com.jakarta.delegate.FinalScreenDelegate;
import br.com.jakarta.delegate.GameOverScreenDelegate;
import br.com.jakarta.delegate.MeteorsEngineDelegate;
import br.com.jakarta.delegate.ShootEngineDelegate;
import br.com.jakarta.layers.FinalScreen;
import br.com.jakarta.layers.GameButton;
import br.com.jakarta.layers.GameOverScreen;
import br.com.jakarta.layers.MeteorsEngine;
import br.com.jakarta.layers.Score;
import br.com.jakarta.sprites.Meteor;
import br.com.jakarta.sprites.Player;
import br.com.jakarta.sprites.ScreenBackground;
import br.com.jakarta.sprites.Shoot;
import br.com.jakarta.utils.Assets;

import static br.com.jakarta.utils.DeviceSettings.BACK_HEIGHT;
import static br.com.jakarta.utils.DeviceSettings.BACK_WIDTH;
import static br.com.jakarta.utils.DeviceSettings.aspectScale;
import static br.com.jakarta.utils.DeviceSettings.screenHeight;
import static br.com.jakarta.utils.DeviceSettings.screenResolution;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class GameScene extends CCScene implements MeteorsEngineDelegate, ShootEngineDelegate,
                                                  FinalScreenDelegate, GameOverScreenDelegate{

    private ScreenBackground background;
    private MeteorsEngine meteorsEngine;
    private GameButton gameButtonControl;
    private Player player;
    private Score score;

    private CCLayer meteorLayer;
    private CCLayer playerLayer;
    private CCLayer shootLayer;
    private CCLayer scoreLayer;

    private List<Meteor> meteorList;
    private List<Shoot> shootList;
    private List<Player> playerList;

    public GameScene(){
        setupBackground();
        SoundEngine.sharedEngine().
                playSound(CCDirector.
                sharedDirector().getActivity(),R.raw.music, true);
        meteorLayer = CCLayer.node();
        playerLayer = CCLayer.node();
        shootLayer = CCLayer.node();
        scoreLayer = CCLayer.node();

        gameButtonControl = GameButton.gameButton();
        gameButtonControl.setDelegate(this);


        addChild(meteorLayer);
        addChild(playerLayer);
        addChild(shootLayer);
        addChild(scoreLayer);
        addChild(gameButtonControl);

        addGameObjects();
        preloadCache();
    }

    private void addGameObjects(){
        meteorList = new ArrayList();
        shootList = new ArrayList<>();
        playerList = new ArrayList<>();

        createPlayer();
        createScore();
    }

    @Override
    public void onEnter() {
        super.onEnter();
        schedule("checkHits");
        startGame();
    }

    @Override
    public void createShoot(Shoot shoot) {
        shoot.setDelegate(this);
        shoot.start();
        shootLayer.addChild(shoot);
        shootList.add(shoot);
    }

    @Override
    public void removeShoot(Shoot shoot) {
        shootList.remove(shoot);
    }

    @Override
    public void createMeteor(Meteor meteor) {
        meteor.setDelegate(this);
        meteorLayer.addChild(meteor);
        meteor.start();
        meteorList.add(meteor);
    }

    @Override
    public void removeMeteor(Meteor meteor) {
        meteorList.remove(meteor);
    }

    private void startEngines() {
        meteorsEngine = new MeteorsEngine();
        addChild(this.meteorsEngine);
        meteorsEngine.setDelegate(this);
    }

    public static CCScene createScene(){
        CCScene scene = CCScene.node();
        GameScene layer = new GameScene();
        scene.addChild(layer);
        return scene;
    }

    private void createScore() {
        score = new Score();
        scoreLayer.addChild(score);
        score.setDelegate(this);
    }

    private void createPlayer() {
        player = new Player();
        player.setGameOverDelegate(this);

        playerList.add(player);
        playerLayer.addChild(player);
    }

    public boolean shoot(){
        player.setDelegate(this);
        player.shoot();
        return true;
    }

    public void moveLeft(){
        player.moveLeft();
    }

    public void moveRight(){
        player.moveRight();
    }

    public void startGame(){
        startEngines();
        initAccelerometer();
    }

    private void initAccelerometer() {
        player.catchAccelerometer();
    }

    public CGRect getBoarders(CCSprite sprite){
        CGRect rect = sprite.getBoundingBox();
        CGPoint glPoint = rect.origin;
        CGRect glRect = CGRect.make(glPoint.x, glPoint.y, rect.size.width, rect.size.height);
        return glRect;
    }

    public void meteorHit(CCSprite meteor, CCSprite shoot){
        ((Meteor)meteor).shooted();
        ((Shoot)shoot).explode();
        score.increase();
    }

    public void playerHit(CCSprite meteor, CCSprite player){
        ((Meteor)meteor).shooted();
        ((Player)player).explode();
    }

    public void checkHits(float dt){
        this.checkRadiusHitsOfArray(meteorList, shootList, this, "meteorHit");
        this.checkRadiusHitsOfArray(meteorList, playerList, this, "playerHit");
    }

    private void setupBackground() {
        float scaleX = screenWidth() / BACK_WIDTH;
        float scaleY = screenHeight() / BACK_HEIGHT;
        background = new ScreenBackground(Assets.BACKGROUND);
        background.setPosition(
                screenResolution(CGPoint.ccp(screenWidth() / 2.0f, screenHeight() / 2.0f)));
        background.setScale(aspectScale(background, scaleX, scaleY));

        addChild(background);
    }

    private boolean checkRadiusHitsOfArray(List<? extends CCSprite> list1,
                                           List<? extends CCSprite> list2,
                                           GameScene gameScene, String hit){

        boolean result = false;
        CGRect rect1, rect2;
        for(int i = 0; i < list1.size(); i++){
            //Recebe o objeto da primeira lista
            rect1 = getBoarders(list1.get(i));
            for(int j = 0; j < list2.size(); j++){
                //Recebe o objeto da primeira lista
                rect2 = getBoarders(list2.get(j));
                //verifica a colisão
                if(CGRect.intersects(rect1, rect2)){
                    result = true;
                    Method method;
                    try {
                        method = GameScene.class.getMethod(hit, CCSprite.class, CCSprite.class);
                        method.invoke(gameScene, list1.get(i), list2.get(j));
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return result;
    }

    private void preloadCache(){
        SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), R.raw.shoot);
        SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), R.raw.bang);
        SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), R.raw.over);
    }

    @Override
    public void startFinalScreen(){
        CCDirector.sharedDirector().replaceScene(CCFadeTransition.transition(1.0f, new FinalScreen().scene()));
    }

    @Override
    public void startGameOverScreen() {
        CCDirector.sharedDirector().replaceScene(CCFadeTransition.transition(1.0f, new GameOverScreen().gameOverScene()));
    }
}