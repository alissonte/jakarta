package br.com.jakarta.scenes;


import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

import br.com.jakarta.layers.MenuButtons;
import br.com.jakarta.sprites.ScreenBackground;
import br.com.jakarta.utils.Assets;

import static br.com.jakarta.utils.DeviceSettings.aspectScale;
import static br.com.jakarta.utils.DeviceSettings.screenHeight;
import static br.com.jakarta.utils.DeviceSettings.screenResolution;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class TitleScreen extends CCLayer{

    public TitleScreen(){
        setupBackground();

        CCSprite title = CCSprite.sprite(Assets.LOGO);
        title.setPosition(
                screenResolution(CGPoint.ccp(screenWidth() / 2.0f, screenHeight() - 130f)));
        this.addChild(title);

        MenuButtons menuLayer = new MenuButtons();
        this.addChild(menuLayer);
    }

    private void setupBackground() {
        ScreenBackground background = new ScreenBackground(Assets.BACKGROUND);
        float scaleX = screenWidth() / 252.63f;
        float scaleY = screenHeight() / 480f;

        background.setPosition(
                screenResolution(CGPoint.ccp(screenWidth() / 2.0f, screenHeight() / 2.0f)));

        background.setScale(aspectScale(background, scaleX, scaleY));
        this.addChild(background);
    }


    public CCScene scene(){
        CCScene scene = CCScene.node();
        scene.addChild(this);
        return scene;
    }
}
