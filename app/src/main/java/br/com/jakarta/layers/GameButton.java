package br.com.jakarta.layers;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.types.CGPoint;

import br.com.jakarta.delegate.ButtonDelegate;
import br.com.jakarta.scenes.GameScene;
import br.com.jakarta.utils.Assets;

import static br.com.jakarta.utils.DeviceSettings.screenResolution;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class GameButton extends CCLayer implements ButtonDelegate {

    private Button leftButton;
    private Button rightButton;
    private Button shootButton;

    private GameScene sceneDelegate;

    public GameButton(){
        setIsTouchEnabled(true);

        leftButton = new Button(Assets.LEFTCONTROL);
        rightButton = new Button(Assets.RIGHTCONTROL);
        shootButton = new Button(Assets.SHOOTBUTTON);

        //configura os delegates
        leftButton.setDelegate(this);
        rightButton.setDelegate(this);
        shootButton.setDelegate(this);

        setButtonPositions();

        //Add buttons on screen
        //addChild(this.leftButton);
        //addChild(this.rightButton);
        addChild(this.shootButton);
    }

    private void setButtonPositions() {
        leftButton.setPosition(screenResolution(CGPoint.ccp(40, 40)));
        rightButton.setPosition(screenResolution(CGPoint.ccp( 100 , 40 )));
        shootButton.setPosition(screenResolution(CGPoint.ccp( screenWidth() -40 , 40 )));
    }

    public static GameButton gameButton(){
        return new GameButton();
    }

    @Override
    public void buttonClicked(Button sender) {
        if(sender.equals(this.leftButton)){
            sceneDelegate.moveLeft();
        }

        if(sender.equals(this.rightButton)){
            sceneDelegate.moveRight();
        }

        if(sender.equals(this.shootButton)){
            sceneDelegate.shoot();
        }
    }

    public void setDelegate(GameScene sceneDelegate){
        this.sceneDelegate = sceneDelegate;
    }
}
