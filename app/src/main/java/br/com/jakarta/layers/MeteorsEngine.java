package br.com.jakarta.layers;


import org.cocos2d.layers.CCLayer;

import java.util.Random;

import br.com.jakarta.delegate.MeteorsEngineDelegate;
import br.com.jakarta.sprites.Meteor;
import br.com.jakarta.utils.Assets;

public class MeteorsEngine extends CCLayer{

    private MeteorsEngineDelegate delegate;

    public MeteorsEngine(){
        this.schedule("meteorEngine", 1.0f / 10f);
    }

    public void meteorEngine(float dt){
        int random = new Random().nextInt(30);
        if(random == 0){
            delegate.createMeteor(new Meteor(Assets.METEOR));
        }
    }

    public void setDelegate(MeteorsEngineDelegate delegate){
        this.delegate = delegate;
    }

    public MeteorsEngineDelegate getDelegate() {
        return delegate;
    }
}
