package br.com.jakarta.layers;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;

import br.com.jakarta.delegate.ButtonDelegate;
import br.com.jakarta.scenes.TitleScreen;
import br.com.jakarta.sprites.ScreenBackground;
import br.com.jakarta.utils.Assets;

import static br.com.jakarta.utils.DeviceSettings.aspectScale;
import static br.com.jakarta.utils.DeviceSettings.screenHeight;
import static br.com.jakarta.utils.DeviceSettings.screenResolution;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class FinalScreen extends CCLayer implements ButtonDelegate{
    private Button beginButton;

    public FinalScreen(){
        setupBackground();
        addTitle();
        configResources();
    }

    private void configResources() {
        setIsTouchEnabled(true);
        beginButton = new Button(Assets.PLAY);
        beginButton.setPosition(CGPoint.ccp(screenWidth() / 2.0f, screenHeight() - 300));
        beginButton.setDelegate(this);
        addChild(beginButton);
    }

    private void addTitle() {
        CCSprite sprite = CCSprite.sprite(Assets.FINALEND);
        sprite.setPosition(screenResolution(
                           CGPoint.ccp(screenWidth()/2.0f,screenHeight() - 130)));
        addChild(sprite);
    }

    private void setupBackground() {
        ScreenBackground background = new ScreenBackground(Assets.BACKGROUND);
        float scaleX = screenWidth() / 252.63f;
        float scaleY = screenHeight() / 480f;

        background.setPosition(screenResolution(CGPoint.ccp(screenWidth() / 2.0f, screenHeight() / 2.0f)));
        background.setScale(aspectScale(background, scaleX, scaleY));
        addChild(background);
    }

    public CCScene scene(){
        CCScene scene = CCScene.node();
        scene.addChild(this);
        return scene;
    }

    @Override
    public void buttonClicked(Button sender) {
        if(sender.equals(beginButton)){
            CCDirector.sharedDirector().replaceScene(CCFadeTransition.transition(1.0f,
                    new TitleScreen().scene()));
        }
    }
}
