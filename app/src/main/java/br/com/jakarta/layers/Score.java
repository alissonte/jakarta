package br.com.jakarta.layers;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.opengl.CCBitmapFontAtlas;

import br.com.jakarta.delegate.FinalScreenDelegate;

import static br.com.jakarta.utils.DeviceSettings.screenHeight;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class Score extends CCLayer{

    private CCBitmapFontAtlas text;

    private FinalScreenDelegate delegate;

    private int score;

    public Score(){
        score = 0;
        text = CCBitmapFontAtlas.bitmapFontAtlas(String.valueOf(score),
               "UniSansSemiBold_Numbers_240.fnt");
        text.setScale((float)240/240);
        setPosition(screenWidth() - 50, screenHeight() - 50);
        addChild(text);
    }

    public void setDelegate(FinalScreenDelegate delegate) {
        this.delegate = delegate;
    }

    public void increase(){
        score++;
        text.setString(String.valueOf(score));
        if(score == 5)
            delegate.startFinalScreen();
    }
}
