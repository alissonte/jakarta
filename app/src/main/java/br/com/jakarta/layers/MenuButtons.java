package br.com.jakarta.layers;


import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;

import br.com.jakarta.delegate.ButtonDelegate;
import br.com.jakarta.scenes.GameScene;
import br.com.jakarta.utils.Assets;

import static br.com.jakarta.utils.DeviceSettings.screenHeight;
import static br.com.jakarta.utils.DeviceSettings.screenResolution;
import static br.com.jakarta.utils.DeviceSettings.screenWidth;

public class MenuButtons extends CCLayer implements ButtonDelegate {

    private Button playButton;
    private Button highScoreButton;
    private Button helpButton;
    private Button soundButton;

    public MenuButtons(){
        setIsTouchEnabled(true);

        playButton = new Button(Assets.PLAY);
        highScoreButton = new Button(Assets.HIGHSCORE);
        helpButton = new Button(Assets.HELP);
        soundButton = new Button(Assets.SOUND);

        playButton.setDelegate(this);
        highScoreButton.setDelegate(this);
        helpButton.setDelegate(this);
        soundButton.setDelegate(this);

        setButtonsPositions();

        addChild(this.playButton);
        addChild(this.highScoreButton);
        addChild(this.helpButton);
        addChild(this.soundButton);
    }

    private void setButtonsPositions() {
        playButton.setPosition(
                screenResolution(CGPoint.ccp(screenWidth() / 2, screenHeight() - 250)));
        highScoreButton.setPosition(
                screenResolution(CGPoint.ccp(screenWidth() / 2, screenHeight() - 320)));
        helpButton.setPosition(
                screenResolution(CGPoint.ccp(screenWidth() / 2, screenHeight() - 390)));
        soundButton.setPosition(
                screenResolution(CGPoint.ccp(screenWidth() / 2 - 100, screenHeight() - 460)));
    }

    @Override
    public void buttonClicked(Button sender) {
        if(sender.equals(this.playButton)){
            CCDirector.sharedDirector().replaceScene(CCFadeTransition.transition(1.0f,
                    GameScene.createScene()));
        }else if(sender.equals(this.highScoreButton)){
        }else if(sender.equals(this.helpButton)){
        }else if(sender.equals(this.soundButton)){
        }
    }
}