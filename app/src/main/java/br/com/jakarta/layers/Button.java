package br.com.jakarta.layers;

import android.view.MotionEvent;

import org.cocos2d.events.CCTouchDispatcher;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import br.com.jakarta.delegate.ButtonDelegate;

public class Button extends CCLayer{

    private CCSprite sprBtnImage;
    private ButtonDelegate delegate;

    public Button(String buttonImage){
        setIsTouchEnabled(true);
        sprBtnImage = CCSprite.sprite(buttonImage);
        addChild(sprBtnImage);
    }

    public void setDelegate(ButtonDelegate delegate){
        this.delegate = delegate;
    }

    @Override
    protected void registerWithTouchDispatcher() {
        CCTouchDispatcher.sharedDispatcher().addTargetedDelegate(this, 0, false);
    }

    @Override
    public boolean ccTouchesBegan(MotionEvent event) {
        CGPoint touchLocation = CGPoint.make(event.getX(), event.getY());
        touchLocation = CCDirector.sharedDirector().convertToGL(touchLocation);
        touchLocation = this.convertToNodeSpace(touchLocation);

        //check if have touch
        if(CGRect.containsPoint(this.sprBtnImage.getBoundingBox(), touchLocation)){
            delegate.buttonClicked(this);
        }
        return true;
    }
}
